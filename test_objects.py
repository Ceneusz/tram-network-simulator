from communication_module import Communication_Module
from objects import TramLine, TramStop


def test_get_stop_by_id():
    test_comm_module = Communication_Module()

    tram1 = TramStop(3, 1, 2)
    tram2 = TramStop(4, 6, 2)
    tram3 = TramStop(98, 23, 3)

    test_comm_module.tram_stops = [tram1, tram2, tram3]
    test_comm_module.dict_stops = {
        3: tram1,
        4: tram2,
        98: tram3
    }

    find = test_comm_module.get_stop_by_id(98)
    find2 = test_comm_module.get_stop_by_id(3)

    assert find == tram3
    assert find2 == tram1


def test_get_line_by_number():
    test_module = Communication_Module()

    tram1 = TramStop(3, 1, 2)
    tram2 = TramStop(4, 6, 2)
    tram3 = TramStop(98, 23, 3)

    line1 = TramLine(6, [tram1, tram2, tram3])
    line2 = TramLine(7, [tram3, tram1, tram2])
    line3 = TramLine(2, [tram2, tram1, tram3])

    test_module.tram_lines = [line1, line2, line3]
    test_module.dict_lines = {
        6: line1,
        7: line2,
        2: line3
    }

    find = test_module.get_line_by_number(7)
    find2 = test_module.get_line_by_number(2)

    assert find == line2
    assert find2 == line3


def test_tram_ride():
    test_module2 = Communication_Module()

    test_module2.load_tram_stops_from_file(
        "test_data/set_of_data/tramstops.csv")
    test_module2.load_tram_connections_from_file(
        "test_data/set_of_data/connections.csv")
    test_module2.load_tram_lines_from_file(
        "test_data/set_of_data/tramlines.csv")
    test_module2.load_trams_from_file("test_data/set_of_data/trams.csv")

    tram = test_module2.trams[0]

    test_module2.tram_ride(tram, 1)

    assert tram.x == 100.0 and tram.y == 100.0


def test_tram_ride_with_2_trams_at_line():

    test_module3 = Communication_Module()

    test_module3.load_tram_stops_from_file(
        "test_data/set_of_data/tramstops.csv")
    test_module3.load_tram_connections_from_file(
        "test_data/set_of_data/connections.csv")
    test_module3.load_tram_lines_from_file(
        "test_data/set_of_data/tramlines.csv")
    test_module3.load_trams_from_file("test_data/set_of_data/trams2.csv")

    tram = test_module3.trams[0]
    tram2 = test_module3.trams[1]

    test_module3.tram_ride(tram, 10)
    test_module3.tram_ride(tram2, 1)

    assert tram2.x == 100.0 and tram2.y == 100.0
    assert tram.x == 150.0 and tram.y == 150.0
