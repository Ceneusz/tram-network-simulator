from communication_module import Communication_Module
from objects import TramStop, TramLine, Tram


def test_load_tramstops():
    test_module = Communication_Module()

    test_module.load_tram_stops_from_file("test_data/stoppies.csv")

    stop1 = TramStop(1, 2, 4, "Heh")
    stop2 = TramStop(2, 4, 8, "Meh")
    stop3 = TramStop(3, 8, 9, "Pech")

    truth = True

    # na przyszłość rozdzielić sprawdzanie danych atrybutów na kilka assertów czy coś

    for arg1, arg2 in zip(test_module.tram_stops, [stop1, stop2, stop3]):
        if (arg1._id != arg2._id or arg1.y != arg2.y or
                arg1.x != arg2.x or arg1.name != arg2.name):
            truth = False

    assert truth


def test_load_tramconn():
    test_module = Communication_Module()

    test_module.load_tram_stops_from_file("test_data/stoppies.csv")
    test_module.load_tram_connections_from_file(
        "test_data/connies.csv"
    )

    test_conn_table = [
                    [0, 4, 0],
                    [4, 0, 2],
                    [0, 2, 0]
                    ]

    assert test_conn_table == test_module.conn_table


def test_load_tramline():
    test_module = Communication_Module()

    test_module.load_tram_stops_from_file("test_data/stoppies.csv")
    test_module.load_tram_connections_from_file(
        "test_data/connies.csv"
    )
    test_module.load_tram_lines_from_file(
        "test_data/linnies.csv"
    )

    stop1 = TramStop(1, 2, 4, "Heh")
    stop2 = TramStop(2, 4, 8, "Meh")
    stop3 = TramStop(3, 8, 9, "Pech")

    test_line = TramLine(1, [stop1, stop2, stop3])

    truth = True

    for arg1, arg2 in zip(test_module.tram_lines[0].stops,
                                                [stop1, stop2, stop3]):
        if (arg1._id != arg2._id or arg1.y != arg2.y or
                arg1.x != arg2.x or arg1.name != arg2.name):
            truth = False

    assert truth and test_line.number == test_module.tram_lines[0].number


def test_load_tram():
    test_module = Communication_Module()

    test_module.load_tram_stops_from_file("test_data/stoppies.csv")
    test_module.load_tram_connections_from_file(
        "test_data/connies.csv"
    )
    test_module.load_tram_lines_from_file(
        "test_data/linnies.csv"
    )
    test_module.load_trams_from_file("test_data/trammies.csv")

    stop1 = TramStop(1, 2, 4, "Heh")
    stop2 = TramStop(2, 4, 8, "Meh")
    stop3 = TramStop(3, 8, 9, "Pech")

    test_line = TramLine(1, [stop1, stop2, stop3])

    test_tram = Tram(0, "Tomek", test_line, 27)

    loaded_tram = test_module.trams[0]

    truth = True

    for arg1, arg2 in zip(loaded_tram.line.stops, test_tram.line.stops):
        if (arg1._id != arg2._id or arg1.y != arg2.y or
                arg1.x != arg2.x or arg1.name != arg2.name):
            truth = False

    assert test_tram.tram_id == loaded_tram.tram_id
    assert test_tram.name == loaded_tram.name
    assert test_tram.velocity == loaded_tram.velocity
    assert test_tram.line.number == loaded_tram.line.number
    assert truth


def test_save():
    test_module = Communication_Module()

    test_module.load_tram_stops_from_file("test_data/stoppies.csv")
    test_module.load_tram_connections_from_file(
        "test_data/connies.csv"
    )
    test_module.load_tram_lines_from_file(
        "test_data/linnies.csv"
    )
    test_module.load_trams_from_file("test_data/trammies.csv")

    tram = test_module.trams[0]

    test_module.tram_ride(tram, 2)

    tram.initiate_save("test_data/saved_data/trams.csv")
    for t_ram in test_module.trams:
        t_ram.save_tram("test_data/saved_data/trams.csv")

    test_module.tram_lines[0].initiate_save("test_data/saved_data/lines.csv")
    for line in test_module.tram_lines:
        line.save_line("test_data/saved_data/lines.csv")

    # zwiększyć elastyczność i dokładność testów

    test_module.tram_stops[0].initiate_save("test_data/saved_data/stops.csv")
    for stop in test_module.tram_stops:
        stop.save_stop("test_data/saved_data/stops.csv")

    test_module.save_tram_conn(
        "test_data/saved_data/connections.csv", ["IDone", "IDtwo", "distance"])

    test_module2 = Communication_Module()

    test_module2.load_tram_stops_from_file("test_data/saved_data/stops.csv")
    test_module2.load_tram_connections_from_file(
        "test_data/saved_data/connections.csv"
    )
    test_module2.load_tram_lines_from_file(
        "test_data/saved_data/lines.csv"
    )
    test_module2.load_trams_from_file("test_data/saved_data/trams.csv")

    truth = True

    for arg1, arg2 in zip(test_module.tram_stops, test_module2.tram_stops):
        if (arg1._id != arg2._id or arg1.x != arg2.x or arg1.y != arg2.y or
                arg1.name != arg2.name):
            truth = False

    for arg1, arg2 in zip(test_module.tram_lines, test_module2.tram_lines):
        if arg1.number != arg2.number:
            truth = False

    for arg1, arg2 in zip(test_module.trams, test_module2.trams):
        if (arg1.tram_id != arg2.tram_id or arg1.x != arg2.x or
                arg1.line.number != arg2.line.number or
                arg1.y != arg2.y or arg1.name != arg2.name or
                arg1.direction != arg2.direction or
                arg1.velocity != arg2.velocity):
            truth = False

    assert truth

# sprawdzić, czy assert działa z pętlą

# setUp, tearDown
