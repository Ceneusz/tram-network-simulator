import pytest
from objects import TramStop, TramLine
from exceptions import (
    NoConnectionError, DataError,
    WrongIdError, SameIdError,
    SameCoordinatesError
)
from communication_module import Communication_Module


def test_DataError():
    test_module = Communication_Module()
    with pytest.raises(DataError):
        test_module.load_tram_stops_from_file("test_data/malformed_stops.csv")


def test_SameIdError():
    test_module2 = Communication_Module()
    with pytest.raises(SameIdError):
        test_module2.load_tram_stops_from_file(
            "test_data/malformed_stops2.csv"
        )


def test_WrongIdError():
    test_module3 = Communication_Module()
    tramstop1 = TramStop(6, 233, 456, "Pszemek")
    tramstop2 = TramStop(2, 233, 126, "Pszemek2")
    tramstop3 = TramStop(5, 555, 234, "Pszemek3")
    tramstop4 = TramStop(3, 1, 3, "Pszemek4")

    test_module3.tram_stops = [tramstop1, tramstop2, tramstop3, tramstop4]

    with pytest.raises(WrongIdError):
        test_module3.get_stop_by_id(4)


def test_WrongIdError2():

    test_modulex = Communication_Module()

    tramstop1 = TramStop(6, 233, 456, "Pszemek")
    tramstop2 = TramStop(2, 233, 126, "Pszemek2")
    tramstop3 = TramStop(5, 555, 234, "Pszemek3")
    tramstop4 = TramStop(3, 1, 3, "Pszemek4")

    test_modulex.tram_lines.append(
        TramLine(2, [tramstop1, tramstop2, tramstop3, tramstop4]))

    with pytest.raises(WrongIdError):
        test_modulex.get_line_by_number(3)


def test_NoConnectionError():
    test_module4 = Communication_Module()
    tramstop1 = TramStop(6, 233, 456, "Pszemek")
    tramstop2 = TramStop(2, 233, 126, "Pszemek2")
    tramstop3 = TramStop(5, 555, 234, "Pszemek3")
    tramstop4 = TramStop(3, 1, 3, "Pszemek4")

    test_module4.tram_stops = [tramstop1, tramstop2, tramstop3, tramstop4]
    test_module4.dict_stops = {
        6: tramstop1,
        2: tramstop2,
        5: tramstop3,
        3: tramstop4
    }

    test_module4.load_tram_connections_from_file(
        "test_data/malformed_conn_table.csv")

    with pytest.raises(NoConnectionError):
        test_module4.load_tram_lines_from_file("test_data/line_example.csv")


def test_SameCoordinatesError():
    test_module5 = Communication_Module()

    with pytest.raises(SameCoordinatesError):
        test_module5.load_tram_stops_from_file(
            "test_data/malformed_tramstops3.csv")
