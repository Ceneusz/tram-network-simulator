class NoConnectionError(Exception):

    """
    Błąd zwracany w przypadku braku zdefiniowanego połączenia,
    między przystankami.
    """

    def __init__(self, message, row=None, inner_exception=None):
        super().__init__(message)
        self.row = row
        self.inner_exception = inner_exception


class DataError(Exception):

    """
    Błąd zwracany w przypadku niepoprawnego formatowania danych.
    """

    def __init__(self, message, row=None, inner_exception=None):
        super().__init__(message)
        self.row = row
        self.inner_exception = inner_exception


class WrongIdError(Exception):

    """
    Błąd zwracany w przypadku, gdy obiekt o danym numerze ID
    nie istnieje.
    """

    def __init__(self, message, row=None, inner_exception=None):
        super().__init__(message)
        self.row = row
        self.inner_exception = inner_exception


class SameIdError(Exception):

    """
    Błąd zwracany w przypadkach powtórzonego numeru ID w danych
    wejściowych.
    """

    def __init__(self, message, row=None, inner_exception=None):
        super().__init__(message)
        self.row = row
        self.inner_exception = inner_exception


class SameCoordinatesError(Exception):

    """
    Błąd zwracany w przypadku istnienia co najmniej dwóch
    przystanków o tych samych koordynatach.
    """

    def __init__(self, message, row=None, inner_exception=None):
        super().__init__(message)
        self.row = row
        self.inner_exception = inner_exception


class NoFileError(Exception):

    """
    Błąd zwracany w przypadku braku plików wejściowych.
    """

    def __init__(self, message, row=None, inner_exception=None):
        super().__init__(message)
        self.row = row
        self.inner_exception = inner_exception
