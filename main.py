from communication_module import Communication_Module
from pyglet_graphics import FirstWindow
from exceptions import DataError, NoFileError
import pyglet
from pyglet.gl import (
    glClear, GL_COLOR_BUFFER_BIT, glColor3f, GL_POINTS, glVertex2f,
    glEnd, glLineWidth, glBegin, GL_LINES, glPointSize, glVertex2i
)
from pyglet.window import key


def main():
    win = FirstWindow(1280, 720, caption='Problem tramwajarzera')

    comm_module = Communication_Module()
    try:
        comm_module.load_tram_stops_from_file("basic_data/tramstops.csv")
        comm_module.load_tram_connections_from_file(
            "basic_data/connections.csv")
        comm_module.load_tram_lines_from_file("basic_data/lines.csv")
        comm_module.load_trams_from_file("basic_data/trams.csv")
    except KeyError:
        raise DataError(
                f"Wrong data format in one of the files."
                )
    except FileNotFoundError as e:
        raise NoFileError(
                f"At least one file doesn't exist.\n{e}"
                )

    if comm_module.is_lacking():
        raise DataError(
                f"Crucial data has not been loaded."
                )

    visibility_constant = len(comm_module.tram_lines)

    @win.event
    def on_draw():

        """
        Funkcja rysująca linię.
        """

        glClear(GL_COLOR_BUFFER_BIT)

        if win.show_line % visibility_constant == 0:
            win.show_all = True
        else:
            win.show_all = False

        win.show_line = win.show_line % visibility_constant

        glPointSize(9999)

        glBegin(GL_POINTS)

        glColor3f(1.000, 0.980, 0.804)

        for x in range(0, 18):
            for y in range(0, 10):
                glVertex2f(x * 67.5, y * 72)

        glEnd()

        glLineWidth(3)

        glBegin(GL_LINES)

        glColor3f(0.2, 0.0, 0.8)

        for line in comm_module.tram_lines:

            index = comm_module.tram_lines.index(line) + 1

            if win.show_all or index == win.show_line:
                for x in range(1, len(line.stops)):
                    przystanek1 = line.stops[x-1]
                    przystanek2 = line.stops[x]
                    glVertex2i(przystanek1.x, przystanek1.y)
                    glVertex2i(przystanek2.x, przystanek2.y)

        glEnd()

        glPointSize(15)

        glBegin(GL_POINTS)

        glColor3f(0.0, 0.0, 1.0)

        for line in comm_module.tram_lines:

            index = comm_module.tram_lines.index(line) + 1

            if win.show_all or index == win.show_line:
                for przystanek in line.stops:
                    glVertex2f(przystanek.x, przystanek.y)

        glEnd()

        glPointSize(12)

        glBegin(GL_POINTS)

        glColor3f(1.0, 0.0, 1.0)

        for tramwaj in comm_module.trams:

            the_line = tramwaj.line
            index = comm_module.tram_lines.index(the_line) + 1

            if win.show_all or index == win.show_line:
                glVertex2f(tramwaj.x, tramwaj.y)

        glEnd()

    @win.event
    def on_key_press(symbol, modifiers):

        """
        Obsługa zdarzeń związanych z naciśnięciem klawiszy klawiatury.
        """

        if symbol == key.ENTER:

            """
            Symulacja ruchu tramwaju.
            """

            for tram in comm_module.trams:
                comm_module.tram_ride(tram, tram.velocity / 30)
            on_draw()
        if symbol == key.LEFT:
            win.show_line -= 1
            on_draw()
        if symbol == key.RIGHT:
            win.show_line += 1
            on_draw()
        if symbol == key.S:

            """
            Zapisywanie obecnego stanu linii.
            """

            comm_module.trams[0].initiate_save("saved_data/trams.csv")
            for tram in comm_module.trams:
                tram.save_tram("saved_data/trams.csv")

            comm_module.tram_lines[0].initiate_save("saved_data/lines.csv")
            for line in comm_module.tram_lines:
                line.save_line("saved_data/lines.csv")

            comm_module.tram_stops[0].initiate_save("saved_data/tramstops.csv")
            for stop in comm_module.tram_stops:
                stop.save_stop("saved_data/tramstops.csv")

            comm_module.save_tram_conn("saved_data/connections.csv", [
                "IDone", "IDtwo", "distance"
            ])
        if symbol == key.L:
            comm_module.clear()
            comm_module.load_tram_stops_from_file("saved_data/tramstops.csv")
            comm_module.load_tram_connections_from_file(
                "saved_data/connections.csv")
            comm_module.load_tram_lines_from_file("saved_data/lines.csv")
            comm_module.load_trams_from_file("saved_data/trams.csv")

            on_draw()

    pyglet.app.run()


if __name__ == "__main__":
    main()
