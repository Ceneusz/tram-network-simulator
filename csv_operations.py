import csv
from exceptions import DataError

"""
Tutaj są klasy, które mają przekazywać dane o przystankach, liniach,
odległościach i tramwajach do modułu komunikacji.
"""


class CsvTramStopReader:

    def __init__(self, stream):
        self.reader = csv.DictReader(stream)

    def read(self):
        result = []
        for row in self.reader:
            result.append(self._create_tramstop(row))
        return result

    def _create_tramstop(self, row):
        try:
            return (
                int(row["ID"]),
                int(row["coor_x"]),
                int(row["coor_y"]),
                row["name"]
            )
        except ValueError:
            raise DataError(
                f"Row {row} is incomplete."
            )


class CsvTramConnReader:

    def __init__(self, stream):
        self.reader = csv.DictReader(stream)

    def read(self):
        result = []

        for row in self.reader:
            if '' in row.values():
                raise DataError(
                    f"At least 1 value missing in row {row}"
                )
            else:
                result.append(row)

        return result


class CsvTramLineReader:

    def __init__(self, stream):
        self.reader = csv.DictReader(stream)

    def read(self):
        result = []
        for row in self.reader:
            line = []
            line.append(int(row["line_number"]))

            stopppies = row["stops_id"].rstrip('"').split(',')

            for numbers in stopppies:
                try:
                    line.append(int(numbers))
                except ValueError:
                    raise DataError(
                        f"Wrong data format for tram lines."
                    )

            result.append(line)

        return result


class CsvTramReader:

    def __init__(self, stream):
        self.reader = csv.DictReader(stream)

    def read(self):
        result = []

        for row in self.reader:
            if None in row.values():
                raise DataError(
                    f"There is at least 1 missing value in row {row.values()}"
                )

            result.append(
                list(row.values())
            )

        return result


class CsvTramWriter:
    def __init__(self, stream, fieldnames):
        self.writer = csv.DictWriter(
            stream, fieldnames=fieldnames)

    def write_header(self):
        self.writer.writeheader()

    def write(self, tram):
        self.writer.writerow(
            {
                "tram_id": tram.tram_id, "tram_name": tram.name,
                "line": tram.line.number, "velocity": tram.velocity,
                "last_x": tram.x, "last_y": tram.y,
                "last_stop": tram.last_stop._id, "direction": tram.direction}
            )


class CsvTramConnWriter:
    def __init__(self, stream, conn_table, stops, fieldnames):
        self.writer = csv.DictWriter(
            stream, fieldnames=fieldnames)
        self.conn_table = conn_table
        self.stops = stops

    def write(self):
        self.writer.writeheader()
        for x in range(0, len(self.conn_table)):
            for y in range(x, len(self.conn_table)):
                if self.conn_table[x][y] != 0:
                    self.writer.writerow(
                        {
                            "IDone": self.stops[x]._id,
                            "IDtwo": self.stops[y]._id,
                            "distance": self.conn_table[x][y]
                        }
                    )


class CsvTramStopWriter:
    def __init__(self, stream, fieldnames):
        self.writer = csv.DictWriter(
            stream, fieldnames=fieldnames)

    def write(self, stop):
        self.writer.writerow(
            {
                "ID": stop._id, "coor_x": stop.x,
                "coor_y": stop.y, "name": stop.name
            })

    def write_header(self):
        self.writer.writeheader()


class CsvTramLineWriter:
    def __init__(self, stream, fieldnames):
        self.writer = csv.DictWriter(
            stream, fieldnames=fieldnames)

    def write_header(self):
        self.writer.writeheader()

    def write(self, line):
        str_stops = []
        for stop in line.stops:
            str_stops.append(str(stop._id))

        schelude = ",".join(str_stops)

        self.writer.writerow(
            {
                "line_number": line.number,
                "stops_id": schelude
            })
