
from csv_operations import (CsvTramWriter, CsvTramLineWriter,
                            CsvTramStopWriter)


class TramStop:

    """
    Obiekt reprezentujący przystanek.
    """

    def __init__(self, _id=-1, x=-1, y=-1, name=""):
        """
        Jest to konstruktor obiektu symulującego przystanek.
        Jego atrybuty konieczne do stworzenia to:
        nazwa (name), numer przystanku (_id) i lista linii,
        które przejeżdżają przez przystanek.
        """
        self.name = name
        self._id = _id
        self.x = x
        self.y = y

    def __str__(self):
        return f"Przystanek: {self.name}\nNumer id: {self._id}"

    def info(self):
        return self.name, self._id, self.x, self.y

    def initiate_save(self, path):
        fieldnames = [
                "ID", "coor_x", "coor_y", "name"
            ]
        with open(path, 'w', newline='') as file:
            CsvTramStopWriter(file, fieldnames).write_header()

    def save_stop(self, path):
        fieldnames = [
                "ID", "coor_x", "coor_y", "name"
            ]
        with open(path, 'a', newline='') as file:
            CsvTramStopWriter(file, fieldnames).write(self)


class TramLine:

    """
    Klasa przechowywująca dane na temat rozkładu linii.
    """

    def __init__(self, number, stops=[]):
        self.number = number
        self.stops = stops

    def __str__(self):
        return f"Linia numer {self.number}\n Przystanki{self.stops}"

    def get_stops(self):
        return self.stops

    def __eq__(self, other):
        return self.number == other.number

    def initiate_save(self, path):
        fieldnames = [
                "line_number", "stops_id"
            ]
        with open(path, 'w', newline='') as file:
            CsvTramLineWriter(file, fieldnames).write_header()

    def save_line(self, path):
        fieldnames = [
                "line_number", "stops_id"
            ]
        with open(path, 'a', newline='') as file:
            CsvTramLineWriter(file, fieldnames).write(self)


class Tram:

    """
    Obiekt symulujący tramwaj, jako argument przy jego
    tworzeniu podajemy jego nazwę i linię. Jego poczatkowe współrzędne
    są zależne od tego do jakiej linii należy.
    """

    def __init__(self, tram_id, name, line, velocity, x=-1, y=-1):
        self.tram_id = tram_id
        self.name = name
        self.line = line
        self.velocity = velocity
        self.last_stop = TramStop()
        self.direction = 1
        self.x = x
        self.y = y

        """
        Direction jest zmienną, która wskazuje, w którą stronę porusza się
        tramwaj. Wynosi 1, jeśli tramwaj jedzie zgodnie z początkowym
        kierunkiem linii albo -1, jeśli w przeciwnym.
        """

    def get_stop(self, x):
        for stop in self.line.stops:
            if stop._id == x:
                return stop

    def initiate_save(self, path):
        fieldnames = [
                "tram_id", "tram_name", "line", "velocity",
                "last_x", "last_y", "last_stop", "direction"
            ]
        with open(path, 'w', newline='') as file:
            CsvTramWriter(file, fieldnames).write_header()

    def save_tram(self, path):
        fieldnames = [
                "tram_id", "tram_name", "line", "velocity",
                "last_x", "last_y", "last_stop", "direction"
            ]
        with open(path, 'a', newline='') as file:
            CsvTramWriter(file, fieldnames).write(self)
