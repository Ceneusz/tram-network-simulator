import pyglet


class FirstWindow(pyglet.window.Window):

    """
    Klasa okna wyświetlanego w programie. Oprócz argumentów,
    które posiada typowy pygletowy obiekt okna, przypisałem mu
    zmienne show_all i show_line, które odpowiadają za
    tryby wyświetlania linii.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.show_all = True
        self.show_line = 0
