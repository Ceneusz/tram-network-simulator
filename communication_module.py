from csv_operations import (
    CsvTramStopReader, CsvTramConnReader, CsvTramLineReader, CsvTramReader,
    CsvTramConnWriter
)
from exceptions import (
    NoConnectionError, DataError, WrongIdError, SameIdError,
    SameCoordinatesError
)
from objects import TramLine, Tram, TramStop
import math


def get_distance(point1, point2):
    return math.sqrt(((point1.x - point2.x)**2) + ((point1.y - point2.y)**2))


def create_table(stops_quantity):
    return [[0 for i in range(stops_quantity)] for j in range(stops_quantity)]


class Communication_Module:

    """
    Serce mojego programu. Będzie ona przechowywać informacje, takie jak:
    - odległości między przystankami
    - dane przystanków
    - dane linii tramwajowych
    - dane poszczególnych tramwajów
    Obiekt ten jest w stanie zapisywać i odczytywać dane oraz symuluje on
    jazdę tramwajem.
    """

    def __init__(self):
        self.tram_stops = []
        self.conn_table = [[]]
        self.tram_lines = []
        self.trams = []

    def load_tram_stops_from_file(self, path):

        """
        Wczytuje przystanki z pliku.
        """

        with open(path, newline="") as file:
            raw_stops = CsvTramStopReader(file).read()

            for line in raw_stops:

                tramstop = TramStop(line[0], line[1],
                                    line[2], line[3])
                self.tram_stops.append(tramstop)

        unique_IDs = set(stop._id for stop in self.tram_stops)

        if len(list(unique_IDs)) != len(self.tram_stops):
            raise SameIdError(
                "At least two stops have the same ID number"
            )

        coords = list((stop.x, stop.y) for stop in self.tram_stops)

        if len(coords) != len(set(coords)):
            raise SameCoordinatesError(
                f"At least 2 stops have the same coordinates."
                )

    def load_tram_connections_from_file(self, path):

        """
        Wczytuje połączenia z pliku
        """

        with open(path, newline="") as file:
            raw_table = CsvTramConnReader(file).read()

        self.conn_table = create_table(len(self.tram_stops))

        for row in raw_table:
            stop1 = self.get_stop_by_id(int(row["IDone"]))
            stop2 = self.get_stop_by_id(int(row["IDtwo"]))

            index1 = self.tram_stops.index(stop1)
            index2 = self.tram_stops.index(stop2)

            if self.conn_table[index1][index2] == 0:
                self.conn_table[index1][index2] = int(row["distance"])
                self.conn_table[index2][index1] = int(row["distance"])
            else:
                raise DataError(
                    f"Stops with IDs {stop1._id} and {stop2._id} had its\
                    connection declared at least two times."
                )

    def load_tram_lines_from_file(self, path):

        """
        Wczytuje linie tramwajowe z pliku.
        """

        with open(path, newline='') as file:
            raw_lines = CsvTramLineReader(file).read()

        for line in raw_lines:
            stoppies = []
            for x in range(1, len(line)):
                stoppies.append(self.get_stop_by_id(line[x]))

            if len(set(stoppies)) != len(stoppies):
                raise SameIdError(
                    f"Same stop appeared at least\
                        twice in a line number {line[0]}."
                )

            for x in range(1, len(stoppies)):
                arg1 = self.tram_stops.index(stoppies[x-1])
                arg2 = self.tram_stops.index(stoppies[x])

                if self.conn_table[arg1][arg2] == 0:
                    raise NoConnectionError(
                        f"No connection between {stoppies[x-1].name}\
                             and {stoppies[x].name}."
                    )

            tramline = TramLine(line[0], stoppies)

            self.tram_lines.append(tramline)

    def load_trams_from_file(self, path):

        """
        Wczytuje tramwaje z pliku. W przypadku wczytywania poprzedniego
        stanu linii, oprócz danych potrzebnych do stworzenia obiektu
        tramwaju, program wczytuje również dane odnośnie kierunku jazdy
        i ostatnio miniętego przystanku.
        """

        with open(path, newline='') as file:
            raw_lines = CsvTramReader(file).read()

        for line in raw_lines:

            if self.get_line_by_number(int(line[2])) is not None:
                tram_line = self.get_line_by_number(int(line[2]))
            else:
                raise WrongIdError(
                    f"Linia numer {line[2]} nie istnieje."
                )

            the_stops = []
            for stop in tram_line.stops:
                the_stops.append(stop)

            the_line = TramLine(tram_line.number, tram_line.stops)

            tram = Tram(
                int(line[0]), line[1],
                the_line, int(line[3])
                )

            tram.x = tram.line.stops[0].x
            tram.y = tram.line.stops[0].y
            tram.last_stop = tram.line.stops[0]

            if len(line) == 8:
                tram.x = float(line[4])
                tram.y = float(line[5])
                tram.last_stop = tram.get_stop(int(line[6]))
                tram.direction = int(line[7])
                if tram.direction == -1:
                    tram.line.stops.reverse()

            self.trams.append(tram)

    def save_tram_conn(self, path, fieldnames):
        with open(path, 'w', newline='') as file:
            CsvTramConnWriter(
                file, self.conn_table, self.tram_stops, fieldnames
            ).write()

    def clear(self):

        """
        Komenda czyszcząca dane obiektu.
        """

        self.trams = []
        self.conn_table = [[]]
        self.tram_stops = []
        self.tram_lines = []

    def get_stop_by_id(self, x):

        """
        Wyszukuje przystanek o numerze ID, podanym, jako
        dana wejściowa.
        """

        for stop in self.tram_stops:
            if stop._id == x:
                return stop

        raise WrongIdError(
            f"Stop with ID number {x} does not exist."
        )

    def get_line_by_number(self, x):

        """
        Wyszukuje linię o numerze podanym na wejściu.
        """

        for line in self.tram_lines:
            if line.number == x:
                return line

        raise WrongIdError(
            f"Line number {x} does not exist."
        )

    def is_lacking(self):

        """
        Funkcja sprawdzająca, czy nie brakuje danych potrzebnych do wykonania
        programu.
        """

        return (len(self.tram_stops) == 0 or len(self.trams) == 0 or
                len(self.conn_table) == 0 or len(self.tram_lines) == 0)

    def tram_ride(self, tram, _range):

        """
        Rekurencyjna komenda, która symuluje jazdę tramwaju. Jako argument
        przyjmuje ilość wirtualnych jednostek odległości, które tramwaj
        pokonałby w ciągu minuty. Gdy pozostaje ich nadwyżka, program
        podstawia ją, jako argument _range do funkcji.
        """

        stop1 = tram.last_stop
        ind = tram.line.stops.index(stop1)
        stop2 = tram.line.stops[ind + tram.direction]

        """
        'stop1' jest ostatnio miniętym przystankiem, a 'stop2' przystankiem
        docelowym. 'stop2' jest następną, bądź poprzednią pozycją na liście,
        w zależności od wartości 'tram.direction'
        """

        distance_coef = get_distance(stop2, tram)/get_distance(stop2, stop1)

        """
        Powyżej liczona jest relacja odległości 'stop2' i tramwaju od
        odległości 'stop2' od 'stop1'.
        """

        arg1 = self.tram_stops.index(self.get_stop_by_id(stop1._id))
        arg2 = self.tram_stops.index(self.get_stop_by_id(stop2._id))

        """
        Powyżej wyznaczane są argumenty tablicy połączeń, aby uzyskać
        odległość 'stop1' od 'stop2' w jednostkach programu. Taka
        implementacja zapewnia działanie, gdy przystanki w danych wejściowych
        nie zostały podane zgodnie z kolejnością związaną z numerami ID.
        """

        converted_dis = distance_coef*self.conn_table[arg1][arg2]

        """
        Powyżej odległość między tramwajem, a 'stop2' konwertowana jest na
        jednostki programu.
        """

        if converted_dis <= _range:

            """
            Gdy trasa, którą jest w stanie pokonać tramwaj jest większa
            od 'converted_dis', tramwaj przenosi się na współrzędne 'stop2',
            a _range jest zapisane w zmiennej ran i pomniejszone o
            'converted_dis'.
            """

            ran = _range
            ran -= converted_dis

            tram.x = stop2.x
            tram.y = stop2.y
            tram.last_stop = stop2

            if stop2._id == tram.line.stops[-1]._id and tram.direction == 1:
                tram.direction *= -1

            elif stop2._id == tram.line.stops[0]._id and tram.direction == -1:
                tram.direction *= -1

            """
            Jeżeli 'stop2' jest ostatnim przystankiem, to tramwaj zmienia
            kierunek jazdy poprzez pomnożenie 'tram.direction' przez -1.
            """

            self.tram_ride(tram, ran)

        else:

            """
            Jeśli tramwaj nie zdołał dotrzeć do 'stop2', następuje koniec
            pętli rekurencyjnej, a tramwaj zmienia swoje położenie.
            """

            tram.x += _range*(stop2.x - tram.x)/converted_dis
            tram.y += _range*(stop2.y - tram.y)/converted_dis
